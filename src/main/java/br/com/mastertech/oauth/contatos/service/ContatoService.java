package br.com.mastertech.oauth.contatos.service;

import br.com.mastertech.oauth.contatos.model.Contato;
import br.com.mastertech.oauth.contatos.repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato addContato(Contato contato) {
        return contatoRepository.save(contato);
    }

    public List<Contato> findByIdUsuario(String idUsuario) {
        return contatoRepository.findByIdUsuario(idUsuario);
    }
}
