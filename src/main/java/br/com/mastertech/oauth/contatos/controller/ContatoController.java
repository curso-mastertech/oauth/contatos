package br.com.mastertech.oauth.contatos.controller;

import br.com.mastertech.oauth.contatos.model.Contato;
import br.com.mastertech.oauth.contatos.model.Usuario;
import br.com.mastertech.oauth.contatos.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping
    public Contato addContato(@AuthenticationPrincipal Usuario usuario, @Valid @RequestBody Contato contato) {
        contato.setNomeUsuario(usuario.getNome());
        contato.setIdUsuario(usuario.getId());
        return contatoService.addContato(contato);
    }

    @GetMapping
    public List<Contato> getContatos(@AuthenticationPrincipal Usuario usuario) {
        return contatoService.findByIdUsuario(usuario.getId());
    }
}
